import { Client, GatewayIntentBits, Partials, TextChannel, Webhook } from 'discord.js';
import { ChatGPTAPI } from 'chatgpt';
import fs from "fs";

const config = JSON.parse(fs.readFileSync("./config.json"));
const prompt = fs.readFileSync("./prompt.txt");

const api = new ChatGPTAPI({
    apiKey: config.openai_key,
    completionParams: {
        model: 'gpt-3.5-turbo',
        // model: 'gpt-4',
        temperature: 1.5,
        max_tokens: 90
    }
})


const hooks = {};
/**
 * 
 * @param {*} channel 
 * @returns {Promise<Webhook>}
 */
async function getWebhook(channel) {
    if (hooks[channel.id]) return hooks[channel.id];

    hooks[channel.id] = await channel.createWebhook(config.webhook);
    return hooks[channel.id];
}


const client = new Client({
    intents: [
        GatewayIntentBits.DirectMessages,
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildBans,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.MessageContent,
    ],
    partials: [Partials.Channel],
});

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('messageCreate', async message => {

    const isSpecialPerson = message.author && message.author.id === config.user_id;
    const isInTestServer = message.guildId && message.guildId === config.test_server_id && !message.author.bot;

    if (!(isInTestServer || isSpecialPerson)) return;

    if (message.content.length < 6 || message.content.length > 700) return;
    if (!message.channel) return;

    const msgIsInThread = message.channel.parent instanceof TextChannel;
    
    const [hook, res] = await Promise.all([
        getWebhook(msgIsInThread ? message.channel.parent : message.channel).then(m => { console.log("Got hook"); return m; }),
        api.sendMessage(`${prompt}\n\n"${message.content}"`).then(m => { console.log("Got msg"); return m; })
    ]);

    const out = res.text.replace(/"/g, "");

    if (res.text.includes("an AI language model") || res.text.includes("ethical")) return console.log("ERORR RESPONSE", res.text);

    if (msgIsInThread) {
        await hook.send({ 
            content: `<@${message.author.id}> ${out}`,
            threadId: message.channel.id
        })
    } else await hook.send(`<@${message.author.id}> ${out}`);
});

client.login(config.discord_token);